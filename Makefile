all: down build migrate up

migrate:
	docker-compose run web pipenv run python src/manage.py migrate $(if $m, app $m,)

makemigrations:
	docker-compose run --volume=${PWD}/src:/src/src web pipenv run python src/manage.py makemigrations
	sudo chown -R ${USER} src/app/migrations/

createsuperuser:
	docker-compose run web python src/manage.py createsuperuser

collectstatic:
	python src/manage.py collectstatic --no-input

dev:
	python src/manage.py runserver localhost:8000

command:
	python src/manage.py ${c}

shell:
	python src/manage.py shell

debug:
	python src/manage.py debug

piplock:
	pipenv install
	sudo chown -R ${USER} Pipfile.lock

lint:
	isort .
	flake8 --config setup.cfg
	black --config pyproject.toml .

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down
#volume прокинуть
migrate_all:
	docker-compose run web python src/manage.py makemigrations app
	docker-compose run web python src/manage.py migrate
	sudo chown -R ${USER} src/app/migrations/

make_all:
	make build
	make up
	make migrate_all
	make createsuperuser

rm_db_volume:
	docker volume rm dt-test_db_volume
