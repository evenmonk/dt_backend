from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db.utils import IntegrityError
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackContext
from app.internal.services import user_service
from app.internal.transport.bot.response_messages import *
from app.internal.transport.bot.utils import get_balance


def start(update: Update, context: CallbackContext):
    telegram_id = update.effective_chat.id

    try:
        user = user_service.add_user(update.effective_chat.username, telegram_id)
    except IntegrityError:
        context.bot.send_message(chat_id=telegram_id, text=BotResponse.ALREADY_REGISTERED.value)
    else:
        context.bot.send_message(chat_id=telegram_id, text=f"{BotResponse.USER_REGISTERED.value}{user.telegram_id}")


def set_phone(update: Update, context: CallbackContext):
    args = update.message.text.split()
    telegram_id = update.effective_chat.id

    if len(args) == 1:
        context.bot.send_message(
            chat_id=telegram_id,
            text=BotResponse.NO_PHONE_NUMBER.value,
        )
        return
    phone_number = args[1]

    try:
        user_service.set_phone(update.effective_chat.id, phone_number)
        message = BotResponse.NUMBER_UPDATED.value
    except ObjectDoesNotExist:
        message = BotResponse.NOT_REGISTERED.value
    except ValidationError:
        message = BotResponse.WRONG_NUMBER.value
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)


def me(update: Update, context: CallbackContext):
    user = user_service.get_user_data_by_username(update.effective_chat.username)
    if not user:
        message = BotResponse.NO_USER.value
    elif not user.phone_number:
        message = BotResponse.NO_PHONE_NUMBER_ASSOCIATED.value
    else:
        message = f"{BotResponse.USER_INFO.value}{str(user)}"
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)


def account_balance(update: Update, context: CallbackContext):
    # get_balance(update, context, "account", user_service.get_user_accounts, 
    #     BotResponse.NO_ACCOUNT.value)

    user = user_service.get_user_data_by_username(update.effective_chat.username)
    if not user:
        context.bot.send_message(chat_id=update.effective_chat.id,
                                text=BotResponse.NO_USER.value)
        return
    user_accounts = user_service.get_user_accounts(user.telegram_id)
    if not len(user_accounts):
        context.bot.send_message(chat_id=update.effective_chat.id,
                                text=BotResponse.NO_ACCOUNT.value)
        return

    # currency = account.currency
    # account_cards = card_service.get_cards_of_account(account)
    # logging.log(logging.INFO, account_cards)
    # card_total_amount = sum(map(lambda card: card.money_amount, account_cards))
    # currency_suffix = ("" if currency == "" else " ") + currency
    # context.bot.send_message(
    #     chat_id=update.effective_chat.id,
    #     text=f"Money amount on account {account_number} is {account.money_amount}"
    #     f"{currency_suffix}.\n"
    #     f"Money amount of all linked cards is {card_total_amount}"
    #     f"{currency_suffix}",
    # )

    keyboard = [[InlineKeyboardButton(str(user_account.account_id), 
                                callback_data=f"account {str(user_account.account_id)}") for
                 user_account in user_accounts]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(BotResponse.CHOOSE_ACCOUNT.value,
                                reply_markup=reply_markup)  


def card_balance(update: Update, context: CallbackContext):
    user = user_service.get_user_data_by_username(update.effective_chat.username)
    if not user:
        context.bot.send_message(chat_id=update.effective_chat.id,
                                text=BotResponse.NO_USER.value)
        return
    user_cards = user_service.get_user_cards(user.telegram_id)
    if not len(user_cards):
        context.bot.send_message(chat_id=update.effective_chat.id,
                                text=BotResponse.NO_CARD.value)
        return
    keyboard = [[InlineKeyboardButton(str(user_card.card_id), 
                                callback_data=f"card {str(user_card.card_id)}") for
                 user_card in user_cards]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(BotResponse.CHOOSE_CARD.value,
                                reply_markup=reply_markup)
    # get_balance(update, context, "card", user_service.get_user_cards, 
    #     BotResponse.NO_CARD.value)
