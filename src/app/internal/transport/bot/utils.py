from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import CallbackContext

from app.internal.services import user_service
from app.internal.transport.bot.response_messages import *


def get_balance(update: Update, context: CallbackContext, item_type, get_function, no_items_message):
    user = user_service.get_user_data_by_username(update.effective_chat.username)
    if not user:
        context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=BotResponse.NO_USER.value)
        return
    items = get_function(user.telegram_id)
    if not len(items):
        context.bot.send_message(chat_id=update.effective_chat.id, text=no_items_message)
        return
    try:
        keyboard = [[InlineKeyboardButton(str(item.number), callback_data=f"{item_type} {str(item.account_id)}") for item in items]]
    except AttributeError:
        keyboard = [[InlineKeyboardButton(str(item.number), callback_data=f"{item_type} {str(item.card_id)}") for item in items]]
    except Exception:
        print(Exception)
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(BotResponse.CHOOSE_ACCOUNT.value, reply_markup=reply_markup)
