from enum import Enum


class BotResponse(Enum):
    ALREADY_REGISTERED = "You are already registered"
    USER_REGISTERED = "User was registered.\nYour telegram id: "
    NO_PHONE_NUMBER = "Set your phone number (10 digits)\nExample: /set_phone 0123456789 "
    NUMBER_UPDATED = "Phone number was updated"
    NOT_REGISTERED = "You're not registered \nUse /start command"
    WRONG_NUMBER = "Wrong phone number"
    NO_USER = "You're not registered \nUse /start and /set_phone commands"
    NO_PHONE_NUMBER_ASSOCIATED = "No phone number is associated with this account\nUse /set_phone command"
    USER_INFO = "Your info:\n"
    NO_ACCOUNT = "You don't have any accounts yet"
    NO_CARD = "You don't have any cards yet"
    CHOOSE_ACCOUNT = "Choose account"
    CHOOSE_CARD = "Choose card"
    NO_ACCOUNT_PROVIDED = "No account number provided"
    NO_CARD_PROVIDED = "No card number provided"
