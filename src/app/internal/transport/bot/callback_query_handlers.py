from app.internal.services import user_service

# def get_balance(update, _):
#     query = update.callback_query.split()
#     item_id = query.data[0]
#     query.answer()
#     if query[0] == "card":
#         balance = user_service.get_balance_for_card(item_id)
#     else:
#         balance = user_service.get_balance_for_account(item_id)
#     query.edit_message_text(text=str(balance))

def card_balance(update, _):
    query = update.callback_query.split()
    card_id = query.data[1]
    query.answer()
    balance = user_service.get_balance_for_card(card_id)
    query.edit_message_text(text=str(balance))


def account_balance(update, _):
    query = update.callback_query.split()
    account_id = query.data[1]
    query.answer()
    balance = user_service.get_balance_for_account(account_id)
    query.edit_message_text(text=str(balance))
