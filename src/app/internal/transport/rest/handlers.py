from django.http import JsonResponse
from django.views import View

from app.internal.services.user_service import get_user_data_by_username
from app.internal.serializers.user_serializer import serialize_user


class UserViewSet(View):

    def get(self, request, telegram_username):
        user = get_user_data_by_username(telegram_username)
        if not user:
            return JsonResponse({"error": "No such user"})
        if not user.phone_number:
            return JsonResponse({"error": "No phone number"})
        return JsonResponse(serialize_user(user))
