import re

from django.core.exceptions import ValidationError
from app.internal.models.user import User
from app.internal.models.account import Account
from app.internal.models.card import Card
from uuid import uuid4
from typing import List


def add_user(telegram_username, telegram_id):
    return User.objects.create(telegram_username=telegram_username, telegram_id=telegram_id)


def set_phone(telegram_id, phone_number):
    if not re.match(r'^\d{10}$', phone_number):
        raise ValidationError('Incorrect phone number')
    user = User.objects.get(telegram_id=telegram_id)
    user.phone_number = phone_number
    user.save()


def get_user_data_by_username(telegram_username):
    return User.objects.get(telegram_username=telegram_username)


def get_user_data_by_uuid(uuid: uuid4) -> User:
    return User.objects.get(telegram_id=uuid)


def get_user_accounts(user_id: uuid4) -> List[Account]:
    return Account.objects.filter(owner=user_id)


def get_user_cards(user_id: uuid4) -> List[Card]:
    return Card.objects.filter(owner=user_id)


def get_balance_for_account(account_id):
    account = Account.objects.get(account_id=account_id)
    # account_cards = card_service.get_cards_of_account(account)
    # logging.log(logging.INFO, account_cards)
    # card_total_amount = sum(map(lambda card: card.money_amount, account_cards))
    # currency_suffix = ("" if currency == "" else " ") + currency
    return f"Account: {account_id}\r\n \
            Balance: {account.balance}"
        # f"{currency_suffix}.\n"
        # f"Money amount of all linked cards is {card_total_amount}"
        # f"{currency_suffix}",


def get_balance_for_card(card_id):
    card = Card.objects.get(card_id=card_id)
    currency = card.currency
    currency_suffix = ("" if currency == "" else " ") + currency
    return f"Money amount on card \
        {card_id} is {card.account.balance}" \
        f"{currency_suffix}.\n"
        # f"Money amount of all linked cards is {card_total_amount}"
        # f"{currency_suffix}",
