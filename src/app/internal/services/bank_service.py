# from typing import List

# from app.internal.dto.account_dto import AccountDTO
# from app.internal.dto.card_dto import CardDTO
from app.internal.models.card import Card
from app.internal.models.account import Account


# class BankService:
#     @staticmethod
#     def get_card(card_dto: CardDTO) -> Card:
#         return Card.objects.get(card_number=card_dto.card_number, account__person_id=card_dto.person_id)

#     @staticmethod
#     def get_account_cards(account_dto: AccountDTO) -> List[Card]:
#         return Card.objects.filter(account__account_id=account_dto.account_id, account__person_id=account_dto.person_id)

def get_card(card_id: str) -> Card:
    return Card.objects.filter(card_id=account_id).first()


def get_bank_account(account_id: str) -> Account:
    return Account.objects.filter(account_id=account_id).first()


def confirm_card(card: Card, telegram_id: int) -> bool:
    return confirm_account(card.owner, telegram_id)


def confirm_bank_account(account: Account, telegram_id: int) -> bool:
    return account.owner.id == telegram_id


# def validate_card_number(id: str) -> bool:
#     return _validate_number(id, Card.DIGITS_COUNT)


# def validate_bank_account_number(account_id: str) -> bool:
#     return _validate_number(account_id, Account.DIGITS_COUNT)


# def _validate_number(number: str, length: int) -> bool:
#     return number.isdigit() and len(number) == length
