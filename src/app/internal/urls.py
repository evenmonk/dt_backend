from django.urls import path

from app.internal.transport.rest.handlers import UserViewSet

urlpatterns = [
    path("me/<str:telegram_username>", UserViewSet.as_view()),
]
