# from rest_framework import serializers
from app.internal.models.user import User


# # Serializers define the API representation.
# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ('telegram_username', 'telegram_id', 'phone_number')

def serialize_user(user: User):
    return {
        "telegram_username": user.telegram_username,
        "telegram_id": user.telegram_id,
        "phone_number": user.phone_number,
    }
