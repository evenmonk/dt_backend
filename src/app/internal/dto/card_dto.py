from app.internal.dto.base_dto import BaseDTO


class CardDTO(BaseDTO):
    def __init__(self, telegram_id: int, card_id: str):
        self.card_id = card_id
        self.telegram_id = telegram_id

    @classmethod
    def validate(cls, card_number: str):
        return card_number.isnumeric()
