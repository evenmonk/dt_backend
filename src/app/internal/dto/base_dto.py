from abc import ABC, abstractmethod

from telegram.ext import CallbackContext


class BaseDTO(ABC):
    @abstractmethod
    def __init__(self, *args):
        pass

    @classmethod
    def validate(cls, *args) -> bool:
        return True

    @classmethod
    def get_instance(cls, telegram_id: int, context: CallbackContext, *args):
        if context.args:
            if cls.validate(*context.args):
                return cls(telegram_id, *context.args, *args)
        raise ValueError
