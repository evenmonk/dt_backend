from app.internal.dto.base_dto import BaseDTO


class AccountDTO(BaseDTO):
    def __init__(self, telegram_id, account_id: str):
        self.account_id = account_id
        self.telegram_id = telegram_id
