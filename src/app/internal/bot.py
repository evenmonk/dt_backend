import inspect
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler
from config.settings import TELEGRAM_BOT_TOKEN
from django.conf import settings
from app.internal.transport.bot import user_handlers, callback_query_handlers

class Bot:
    def __init__(self):
        self.updater = Updater(token=TELEGRAM_BOT_TOKEN)
        self.add_handlers()
        self.add_callback_queries_handlers()

    def run(self):
        self.updater.start_polling()
        self.updater.idle()

    def add_handlers(self):
        handlers_functions = inspect.getmembers(user_handlers, 
                                            inspect.isfunction)
        dispatcher = self.updater.dispatcher
        for handler_function in handlers_functions:
            current_handler = CommandHandler(handler_function[0], handler_function[1])
            dispatcher.add_handler(current_handler)

    def add_callback_queries_handlers(self):
            callback_queries_handlers = inspect.getmembers(callback_query_handlers,
                                                    inspect.isfunction)
            dispatcher = self.updater.dispatcher
            for handler_function in callback_queries_handlers:
                current_handler = CallbackQueryHandler(handler_function[1],
                                            pattern=handler_function[0])
                dispatcher.add_handler(current_handler)
