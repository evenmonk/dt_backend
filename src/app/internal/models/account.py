import uuid

from django.db import models
from django.utils.timezone import now
from app.internal.models.user import User


class Account(models.Model):
    account_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    open_date = models.DateTimeField(null=False, default=now)
    balance = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    number = models.CharField(max_length=16, unique=True, null=False)

    def __str__(self):
        return str(self.account_id)
