import uuid

from django.db import models
from app.internal.models.account import Account
from app.internal.models.user import User


class Card(models.Model):
    card_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False, max_length=16)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    currency = models.CharField(max_length=3, null=False)
    balance = models.DecimalField(max_digits=20, decimal_places=2)
    number = models.CharField(max_length=16, unique=True, null=False)

    def display_account(self):
        return self.account.account_id

    display_account.short_description = "account"

    def __str__(self):
        return str(self.card_id)

    def msg_str(self):
        return f"Card: {self.card_number}\n{self.currency}: {self.balance}"
