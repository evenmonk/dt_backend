FROM python:3.10-slim-buster

ENV PYTHONDONTWRITEBYTECODE=1 \
	PYTHONUNBUFFERED=1

WORKDIR /src

COPY Pipfile Pipfile.lock /src/

RUN pip3 install pipenv && \
	pipenv sync --clear && \
	rm -rf /var/lib/apt/lists/*

COPY . .